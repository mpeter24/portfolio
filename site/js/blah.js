$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});

$(document).ready(function() {
var stickyNavTop = $('.intro1').offset().top;

var stickyNav = function(){
var scrollTop = $(window).scrollTop();

if (scrollTop > stickyNavTop) {
    $('.intro1').addClass('sticky');
} else {
    $('.intro1').removeClass('sticky');
}
};

stickyNav();

$(window).scroll(function() {
  stickyNav();
});
});

/*------------------------------------------------*/
